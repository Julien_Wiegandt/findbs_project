from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from PIL import Image

class MyAccountManager(BaseUserManager):
    def create_user(self, email, username, city,  password=None):
        if not email:
            raise ValueError("Users must have an email adress")
        if not username:
            raise ValueError("Users must have an username")
        if not city:
            raise ValueError("Users must have a city")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            city=city,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, city, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
            city=city,
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self.db)
        return user

class Account(AbstractBaseUser):
    email           = models.EmailField(verbose_name="email", max_length=60, unique=True)
    username        = models.CharField(max_length=30, unique=True)
    date_joined     = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login      = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_admin        = models.BooleanField(default=False)
    is_active       = models.BooleanField(default=True)
    is_staff        = models.BooleanField(default=False)
    is_superuser    = models.BooleanField(default=False)
    phone           = models.CharField(max_length=10, null=True)
    city            = models.CharField(max_length=30)
    isparent        = models.BooleanField(verbose_name='are you Parent', default=True)
    description     = models.CharField(max_length=200, null=True, default="")
    remuneration    = models.IntegerField(default=0)
    positiveGrade   = models.IntegerField(default=0)
    negativeGrade   = models.IntegerField(default=0)
    image           = models.ImageField(default='default.png', upload_to='profile_pics')

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username", "city"]

    objects = MyAccountManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True

    def save(self, *args, **kwargs):
        super().save()

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
