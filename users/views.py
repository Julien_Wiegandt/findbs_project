from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings
from .forms import RegisterForm, ParentForm, BSForm
from users.models import Account


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid():
            save_it = form.save()
            
            #Email register message
            subject = "Welcome to FindBS"
            message = "Hello "+save_it.username+"!\n\nYou are registred on FindBS as a "
            if(save_it.isparent):
                message += "parent"
            else:
                message += "baby-sitter"
            message += ", so you can log in now to access your features:\n- Personnalize you profile\n"
            if(save_it.isparent):
                message += "- Request a babysitting slots in the Need section\n- Note the babysitters you have been in contact with"
            else:
                message += "- Find babysitting slots in the Find section\n- Note the parents you have been in contact with"

            message += "\n\nLogin to your account to find out all about it =)"
            message += "\n\nFindBS : findbs.herokuapp.com/users/login/"
            from_email = settings.EMAIL_HOST_USER
            to_list = [save_it.email, settings.EMAIL_HOST_USER]
            send_mail(subject, message, from_email, to_list, fail_silently=True)

            messages.success(request, 'Your account has been created! You are now able to log in')
            return redirect('users-login')
    else:
        form = RegisterForm()
    context = {
        'form': form,
    }
    return render(request, 'users/register.html', context)


@login_required
def profile(request):
    if request.method == 'POST':
        if request.user.isparent :
            form = ParentForm(request.POST, request.FILES, instance=request.user)
        else:
            form = BSForm(request.POST, request.FILES, instance=request.user)

        if form.is_valid():
            username_valid = True
            email_valid = True

            if Account.objects.filter(username=form.cleaned_data['username']).exclude(id=request.user.id).count()>0:
                username_valid = False

            if Account.objects.filter(email=form.cleaned_data['email']).exclude(id=request.user.id).count()>0:
                email_valid = False

            if username_valid == False:
                messages.error(request, 'This username is already use!')
            elif email_valid == False:
                messages.error(request, 'This email adress is already use!')
            else:
                form.save()
                messages.success(request, 'Your account has been updated!')

            return redirect('users-profile')
    else:
        if request.user.isparent :
            form = ParentForm(instance=request.user)
        else:
            form = BSForm(instance=request.user)

    context = {
        'form': form,
    }
    return render(request, 'users/profile.html', context)