from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Account


class RegisterForm(UserCreationForm):
    isparent        = forms.BooleanField(label="Parent mode", required=False)
    email           = forms.EmailField()
    city            = forms.CharField(widget=forms.TextInput(attrs={'id': 'profile_city'}))
    
    def clean(self):
        valid_email = False
        valid_username = False
        email = self.cleaned_data['email']
        username = self.cleaned_data['username']

        try:
            Account.objects.get(username=username)
        except:
            valid_username = True

        try:
            Account.objects.get(email=email)
        except:
            valid_email = True

        if valid_username == False:
            raise forms.ValidationError("This username is already use!")

        if valid_email == False:
            raise forms.ValidationError("This email adress is already use!")

        self.cleaned_data['city'] = self.cleaned_data['city'].upper()
        return self.cleaned_data

    class Meta:
        model = Account
        fields = ['username', 'email', 'city', 'isparent', 'password1', 'password2']


class BSForm(forms.ModelForm):
    phone           = forms.CharField(required=False)
    isparent        = forms.BooleanField(label="Parent mode", required=False)
    description     = forms.CharField(required=False)
    remuneration    = forms.IntegerField(required=False)
    city            = forms.CharField(widget=forms.TextInput(attrs={'id': 'profile_city'}))

    def clean(self):
        self.cleaned_data['city'] = self.cleaned_data['city'].upper()
        return self.cleaned_data

    class Meta:
        model = Account
        fields = ['username', 'email', 'phone', 'city', 'isparent', 'description','remuneration', 'image']

class ParentForm(forms.ModelForm):
    phone           = forms.CharField(required=False)
    isparent        = forms.BooleanField(label="Parent mode", required=False)
    description     = forms.CharField(required=False)
    city            = forms.CharField(widget=forms.TextInput(attrs={'id': 'profile_city'}))

    class Meta:
        model = Account
        fields = ['username', 'email', 'phone', 'city', 'isparent', 'description', 'image']

    def clean(self):
        self.cleaned_data['city'] = self.cleaned_data['city'].upper()
        return self.cleaned_data