from django.shortcuts import render

links=[
    {
        'img1': 'http://www.ligne-de-mire.fr/wp-content/uploads/2019/08/baby-sitter-1024x684.jpg',
        'img2': 'https://www.portail-autoentrepreneur.fr/media/knowledgebase/baby_sitter_auto_entrepreneur.jpg',
        'img3': 'https://shootingpourfemme.files.wordpress.com/2019/09/babysitter.jpg',
        'img4': 'https://i2.wp.com/www.airnounou.com/wp-content/uploads/2018/01/babysitter-parent-sitter.jpg?w=676&ssl=1'
    }
]

def home(request):
    context = {
        'links': links[0]
    }
    return render(request, 'main/home.html', context)
