from django.conf.urls import url
from availability.views import availability_list, availability_create, availability_update, availability_delete, availability_find

urlpatterns = [
    url(r'^$', availability_list, name="availability_list"),
    url(r'^availability_update/(\d+)/$', availability_update, name="availability_update"),
    url(r'^availability_delete/(\d+)/$', availability_delete, name="availability_delete"),
    url(r'^availability_create/$', availability_create, name="availability_create"),
    url(r'^availability_find/$', availability_find, name="availability_find"),
]