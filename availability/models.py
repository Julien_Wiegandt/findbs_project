from django.db import models
from users.models import Account
from django.urls import reverse

class Availability(models.Model):
    id          = models.AutoField(primary_key=True, blank=True)
    user        = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    day         = models.DateField(null=True)
    beghour     = models.TimeField(auto_now=False, auto_now_add=False)
    endhour     = models.TimeField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return self.user.city+" "+str(self.day)
        
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)