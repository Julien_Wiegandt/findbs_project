from django.shortcuts import render, redirect, get_object_or_404
from .models import Availability
from .forms import AvailabilityForm
from datetime import date
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from availability.models import Availability
from reservation.models import Reservation
from .forms import FindForm

@login_required
def availability_list(request):
    availabilitys = Availability.objects.filter(user=request.user, day__gte=date.today()).order_by('day')
    return render(request, 'availability/availability_list.html', {'availabilitys': availabilitys})

@login_required
def availability_create(request):
    if request.POST:
        form = AvailabilityForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            messages.success(request, 'Need created!')
            return redirect('availability_list')
    else:
        form = AvailabilityForm()
    return render(request, 'availability/availability_create.html', {'form': form})

@login_required
def availability_update(request, id_availability):
    availability = get_object_or_404(Availability, id=id_availability)
    form = AvailabilityForm(request.POST or None, instance=availability)
    if form.is_valid():
        form.save()
        messages.success(request, 'Need updated!')
        return redirect('availability_list')
    return render(request, "availability/availability_create.html", {'form': form})

@login_required
def availability_delete(request, id_availability):
    availability = Availability.objects.get(id=id_availability)
    availability.delete()
    messages.success(request, 'Need deleted!')
    return redirect('availability_list')

@login_required
def availability_find(request):
    if request.method == 'POST':
        form = FindForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            day = instance.day
            already_reserved = Reservation.objects.values('availability')#Toute les availability déjà réservé
            all_request = []
            all_request.append(Availability.objects.filter(day=day,user__city=request.user.city))
            for i in already_reserved:
                for key in i:
                    all_request.append(Availability.objects.filter(day=day,user__city=request.user.city).exclude(id=i[key]))
            availabilitys = all_request[0]
            for i in all_request:
                availabilitys = availabilitys & i
            return render(request, 'availability/availability_find.html', {'availabilitys': availabilitys, 'form': form, 'day':day})
    
    already_reserved = Reservation.objects.values('availability')#Toute les availability déjà réservé
    all_request = []
    all_request.append(Availability.objects.filter(user__city=request.user.city, day__gte=date.today()).order_by('day'))
    for i in already_reserved:
        for key in i:
            all_request.append(Availability.objects.filter(user__city=request.user.city).exclude(id=i[key]))
    availabilitys = all_request[0]
    for i in all_request:
        availabilitys = availabilitys & i
    form = FindForm()
    return render(request, 'availability/availability_find.html', {'availabilitys': availabilitys, 'form': form})