from django import forms
from .models import Availability
import datetime
import re

class AvailabilityForm(forms.ModelForm):
    day = forms.DateField(widget=forms.TextInput({ "placeholder": "MM/DD/YY", 'id': 'tamere' }))
    beghour = forms.TimeField(widget=forms.TextInput({ "placeholder": "HH:MM" }), label="Beginning time")
    endhour = forms.TimeField(widget=forms.TextInput({ "placeholder": "HH:MM" }), label="Ending time")

    def clean(self):
        cleaned_data = super().clean()
        print(cleaned_data)
        day = cleaned_data.get("day")
        beghour = cleaned_data.get("beghour")
        endhour = cleaned_data.get("endhour")

        pattern = '^[0-9]{2,4}-[0-9]{1,2}-[0-9]{1,2}$'
        resultdate = re.match(pattern, str(day))
        pattern = '^[0-9]{2}:[0-9]{2}:[0-9]{2}$'
        resultbeg = re.match(pattern, str(beghour))
        resultend = re.match(pattern, str(endhour))
        

        if(resultdate and resultbeg and resultend):
            if day < datetime.date.today():
                raise forms.ValidationError("The date cannot be in the past!")

            if endhour < beghour:
                raise forms.ValidationError("The ending time cannot be before de beginning time!")
        
        return cleaned_data

    class Meta:
        model = Availability
        fields = ['day', 'beghour', 'endhour']
    

class FindForm(forms.ModelForm):
    day = forms.DateField(widget=forms.TextInput({ "placeholder": "MM/DD/YY" }))
    class Meta:
        model = Availability
        fields = ['day']

    def clean(self):
        cleaned_data = super().clean()
        day = cleaned_data.get("day")

        pattern = '^[0-9]{2,4}-[0-9]{1,2}-[0-9]{1,2}$'
        result = re.match(pattern, str(day))
        if(result):
            if day < datetime.date.today():
                raise forms.ValidationError("The date cannot be in the past!")
        
        return cleaned_data