from django.db import models
from users.models import Account
from availability.models import Availability
from django.urls import reverse

state = [
    ("None", "None"),
    ("Parent", "Parent"), 
    ("Baby-sitter", "Baby-sitter"),
]

class Reservation(models.Model):
    id              = models.AutoField(primary_key=True, blank=True)
    availability    = models.ForeignKey(Availability, null=True, on_delete=models.CASCADE)
    bs          = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    ratedstate     = models.CharField(choices=state, max_length=30, default="None")

    def __str__(self):
        return "BS: "+self.availability.user.username+" Reserved on: "+str(self.availability.day)+" with : "+self.bs.username
        
    def save_model(self, request, obj, form, change):
        obj.parent = request.user
        super().save_model(request, obj, form, change)