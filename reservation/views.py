from django.shortcuts import render, redirect, get_object_or_404
from .models import Reservation
from availability.models import Availability
from users.models import Account
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.decorators import login_required
from datetime import date
from demande.models import Demande

@login_required
def reservation_list(request):
    count = 0
    if request.user.isparent:
        reservations = Reservation.objects.filter(availability__user=request.user, availability__day__gte=date.today()).order_by('availability__day')
        demandes = Demande.objects.filter(availability__user=request.user, availability__day__gte=date.today()).order_by('availability__day')

        for i in demandes:
            count += 1
        
    else:
        reservations = Reservation.objects.filter(bs=request.user, availability__day__gte=date.today()).order_by('availability__day')
        demandes = Demande.objects.filter(bs=request.user, availability__day__gte=date.today()).order_by('availability__day')
        for i in demandes:
            count += 1
    
    return render(request, 'reservation/reservation_list.html', {'reservations': reservations, 'demandes': demandes, 'count': count})

@login_required
def reservation_create(request, id_availability, id_demande):
    availability = Availability.objects.get(id=id_availability)
    demande = Demande.objects.get(id=id_demande)
    demande.delete()
    reservation = Reservation(availability=availability, bs=demande.bs)
    reservation.save()

    subject = "Request for babysitting on "+str(demande.availability.day)+" ACCEPTED"
    message = "Hello "+demande.bs.username+"!\n\n"+request.user.username+" has accepted your request for babysitting on "+str(demande.availability.day)+" from "+str(demande.availability.beghour)+" to "+str(demande.availability.endhour)+"."
    message += "\n\nYou can view your planned babysitting in the Reservation section.\n\nFindBS: findbs.herokuapp.com/reservation/"
    from_email = settings.EMAIL_HOST_USER
    to_list = [demande.bs.email, settings.EMAIL_HOST_USER]
    send_mail(subject, message, from_email, to_list, fail_silently=True)

    return redirect('reservation_list')

@login_required
def reservation_delete(request, id_reservation):
    reservation = Reservation.objects.get(id=id_reservation)
    reservation.delete()
    return redirect('reservation_list')

@login_required
def reservation_cancel(request, id_reservation):
    reservation = Reservation.objects.get(id=id_reservation)
    reservation.delete()
    
    subject = "Reservation on "+str(reservation.availability.day)+" CANCELLED"
    if request.user.isparent:
        message = "Hello "+reservation.bs.username+"!\n\n"+request.user.username+" has canceled the babysitting scheduled for "+str(reservation.availability.day)+" from "+str(reservation.availability.beghour)+" to "+str(reservation.availability.endhour)+"."
        message += "\n\nYou can look for another babysitting job to do that day in the Find section.\n\nFindBS: findbs.herokuapp.com/availability/availability_find/"
        from_email = settings.EMAIL_HOST_USER
        to_list = [reservation.bs.email, settings.EMAIL_HOST_USER]
        send_mail(subject, message, from_email, to_list, fail_silently=True)
    else:
        message = "Hello "+reservation.availability.user.username+"!\n\n"+request.user.username+" is no longer available for babysitting on "+str(reservation.availability.day)+" from "+str(reservation.availability.beghour)+" to "+str(reservation.availability.endhour)+"."
        message +="\n\nYour babysitting niche is yet again visible to all babysitters."
        from_email = settings.EMAIL_HOST_USER
        to_list = [reservation.availability.user.email, settings.EMAIL_HOST_USER]
        send_mail(subject, message, from_email, to_list, fail_silently=True)


    return redirect('reservation_list')


@login_required
def reservation_done(request):
    count = 0
    users = Account.objects.filter(city=request.user.city).exclude(username=request.user.username).order_by('-positiveGrade')
    if request.user.isparent:
        reservations = Reservation.objects.filter(availability__user=request.user, availability__day__lt=date.today()).exclude(ratedstate="Parent").order_by('availability__day')
        
    else:
        reservations = Reservation.objects.filter(bs=request.user, availability__day__lt=date.today()).exclude(ratedstate="Baby-sitter").order_by('availability__day')
    
    for i in reservations:
            count += 1
    
    context = {
        'reservations': reservations, 
        'count': count,
        'users': users
        }
    
    return render(request, 'reservation/reservation_done.html',context)

def add_positive_grade(request, id_reservation):

    if request.user.isparent:
        bs = Reservation.objects.filter(id=id_reservation).values('bs')               #Le baby-sitter de la disponibilitée visée
        account = Account.objects.filter(id=bs[0]['bs'])                              #Le compte du baby-sitter
        grade = Account.objects.filter(id=bs[0]['bs']).values('positiveGrade')        #La note+ du BS
        account.update(positiveGrade =grade[0]['positiveGrade']+1)                    #Ajout de 1 à la note

        state = Reservation.objects.filter(id=id_reservation).values('ratedstate')
        if(state[0]['ratedstate'] == "None"):
            Reservation.objects.filter(id=id_reservation).update(ratedstate="Parent")
        else:
            availability = Reservation.objects.filter(id=id_reservation).values('availability')
            Availability.objects.filter(id=availability[0]['availability']).delete()

    else:
        availability = Reservation.objects.filter(id=id_reservation).values('availability')
        parent = Availability.objects.filter(id=availability[0]['availability']).values('user')
        grade = Account.objects.filter(id=parent[0]['user']).values('positiveGrade')
        account = Account.objects.filter(id=parent[0]['user'])
        account.update(positiveGrade =grade[0]['positiveGrade']+1)

        state = Reservation.objects.filter(id=id_reservation).values('ratedstate')
        if(state[0]['ratedstate'] == "None"):
            Reservation.objects.filter(id=id_reservation).update(ratedstate="Baby-sitter")
        else:
            availability = Reservation.objects.filter(id=id_reservation).values('availability')
            Availability.objects.filter(id=availability[0]['availability']).delete()
    

    return redirect('reservation_done')

def add_negative_grade(request, id_reservation):
    if request.user.isparent:
        bs = Reservation.objects.filter(id=id_reservation).values('bs')
        account = Account.objects.filter(id=bs[0]['bs'])
        grade = Account.objects.filter(id=bs[0]['bs']).values('negativeGrade')
        account.update(negativeGrade =grade[0]['negativeGrade']+1)

        state = Reservation.objects.filter(id=id_reservation).values('ratedstate')
        if(state[0]['ratedstate'] == "None"):
            Reservation.objects.filter(id=id_reservation).update(ratedstate="Parent")
        else:
            availability = Reservation.objects.filter(id=id_reservation).values('availability')
            Availability.objects.filter(id=availability[0]['availability']).delete()

    else:
        availability = Reservation.objects.filter(id=id_reservation).values('availability')
        parent = Availability.objects.filter(id=availability[0]['availability']).values('user')
        grade = Account.objects.filter(id=parent[0]['user']).values('negativeGrade')
        account = Account.objects.filter(id=parent[0]['user'])
        account.update(negativeGrade =grade[0]['negativeGrade']+1)

        state = Reservation.objects.filter(id=id_reservation).values('ratedstate')
        if(state[0]['ratedstate'] == "None"):
            Reservation.objects.filter(id=id_reservation).update(ratedstate="Baby-sitter")
        else:
            availability = Reservation.objects.filter(id=id_reservation).values('availability')
            Availability.objects.filter(id=availability[0]['availability']).delete()

        
    return redirect('reservation_done')