from django.conf.urls import url
from .views import reservation_list, reservation_delete, reservation_create, reservation_done, add_positive_grade, add_negative_grade, reservation_cancel

urlpatterns = [
    url(r'^$', reservation_list, name="reservation_list"),
    url(r'^reservation_done/$', reservation_done, name="reservation_done"),
    url(r'^add_positive_grade/(\d+)/$', add_positive_grade, name="add_positive_grade"),
    url(r'^add_negative_grade/(\d+)/$', add_negative_grade, name="add_negative_grade"),
    url(r'^reservation_delete/(\d+)/$', reservation_delete, name="reservation_delete"),
    url(r'^reservation_cancel/(\d+)/$', reservation_cancel, name="reservation_cancel"),
    url(r'^reservation_create/(\d+)/(\d+)/$', reservation_create, name="reservation_create"),
]
