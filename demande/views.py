from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.decorators import login_required
from availability.models import Availability
from .models import Demande


@login_required
def demande_create(request, id_availability):
    availability = Availability.objects.get(id=id_availability)
    demande = Demande(availability=availability, bs=request.user)
    demande.save()

    subject = "Request for babysitting on "+str(availability.day)
    message = "Hello "+availability.user.username+"!\n\n"+demande.bs.username+" is offering to be your babysitter on "+str(availability.day)+" from "+str(availability.beghour)+" to "+str(availability.endhour)+"."
    message += "\n\nYou can accept or refuse his request in your Reservation section.\n\nFindBS : findbs.herokuapp.com/reservation/"
    from_email = settings.EMAIL_HOST_USER
    to_list = [availability.user.email, settings.EMAIL_HOST_USER]
    send_mail(subject, message, from_email, to_list, fail_silently=True)

    return redirect('reservation_list')

@login_required
def demande_delete(request, id_demande):
    demande = Demande.objects.get(id=id_demande)
    demande.delete()

    subject = "Request for babysitting on "+str(demande.availability.day)+" REFUSED"
    message = "Hello "+demande.bs.username+"!\n\n"+request.user.username+" refused your request for babysitting on "+str(demande.availability.day)+" from "+str(demande.availability.beghour)+" to "+str(demande.availability.endhour)+"."
    message += "\n\nYou can look for another babysitting job to do that day in the Find section.\n\nFindBS: findbs.herokuapp.com/availability/availability_find/"
    from_email = settings.EMAIL_HOST_USER
    to_list = [demande.bs.email, settings.EMAIL_HOST_USER]
    send_mail(subject, message, from_email, to_list, fail_silently=True)

    return redirect('reservation_list')

@login_required
def demande_cancel(request, id_demande):
    demande = Demande.objects.get(id=id_demande)
    demande.delete()

    subject = "Request for babysitting on "+str(demande.availability.day)+" CANCELLED"
    message = "Hello "+demande.availability.user.username+"!\n\n"+request.user.username+" canceled his request for babysitting on "+str(demande.availability.day)+" from "+str(demande.availability.beghour)+" to "+str(demande.availability.endhour)+"."
    from_email = settings.EMAIL_HOST_USER
    to_list = [demande.availability.user.email, settings.EMAIL_HOST_USER]
    send_mail(subject, message, from_email, to_list, fail_silently=True)

    return redirect('reservation_list')

@login_required
def demande_list(request, id_availability):
    count = 0
    if request.user.isparent:
        demandes = Demande.objects.filter(availability__user=request.user, availability__day__gte=date.today()).order_by('availability__day')
        for i in demandes:
            count += 1
        
    else:
        demandes = Demande.objects.filter(bs=request.user, availability__day__gte=date.today()).order_by('availability__day')
        for i in demandes:
            count += 1
    
    return render(request, 'reservation/reservation_list.html', {'demandes': demandes, 'count': count})