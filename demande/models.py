from django.db import models
from users.models import Account
from availability.models import Availability

class Demande(models.Model):
    id              = models.AutoField(primary_key=True, blank=True)
    availability    = models.ForeignKey(Availability, null=True, on_delete=models.CASCADE)
    bs          = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    accepted     = models.CharField(default="Neutre", max_length=20)
