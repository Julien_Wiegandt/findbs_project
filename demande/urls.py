from django.conf.urls import url
from .views import demande_create, demande_delete, demande_list, demande_cancel

urlpatterns = [
    url(r'^$', demande_list, name="demande_list"),
    url(r'^demande_delete/(\d+)/$', demande_delete, name="demande_delete"),
    url(r'^demande_cancel/(\d+)/$', demande_cancel, name="demande_cancel"),
    url(r'^demande_create/(\d+)/$', demande_create, name="demande_create"),
]